---
layout: page
title: Swoole Drush
permalink: /swoole/drush/
---
## Controling the (Open)Swoole server with Drush

The (Open)Swoole server is started with the Drush command:

`drush swoole:start`

A running server can be restarted with the Drush command:

`drush swoole:restart`

Command options for the start and the restart commands:
 - `cr` to rebuild the cache before (re)starting the server
 - `host` to override the host for the Swoole server setting from config, which defaults to `localhost`
 - `port` to override the port for the Swoole server setting from config, which defaults to `8000`

The server is stopped with the Drush command:

`drush swoole:stop`
