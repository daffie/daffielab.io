---
layout: page
title: Swoole Installation
permalink: /swoole/installation/
---
## Installation


### Installing the Swoole PHP extension

The (Open)Swoole PHP extension can be installed by:

`pecl install swoole`

or

`pecl install openswoole`


### Installing the Swoole module

The module needs to be installed via the Composer package manager, as the file "bin/swoole-server" needs to be autoloaded.

`composer require daffie/swoole`

After that the module needs to be [enabled](https://www.drupal.org/docs/extending-drupal/installing-modules#s-step-2-enable-the-module).


### Serving your website

You can run Drupal website directly via the (Open)Swoole server or you can serve it behind a traditional web server like Nginx or Apache. Doing so will allow the web server to serve your static assets such as images and stylesheets, as well as manage your SSL certificate termination.

In the directory .ddev of the swoole module are example files of how to get the Swoole PHP extension to work and how to get it to work with Nginx.

For DDEV with Nginx change:
```
location / {
    absolute_redirect off;
    try_files $uri $uri/ /index.php?$query_string;
}

location @rewrite {
    # For D7 and above:
    # Clean URLs are handled in drupal_environment_initialize().
    rewrite ^ /index.php;
}

# pass the PHP scripts to FastCGI server listening on socket
location ~ \.php$ {
    try_files $uri =404;
    fastcgi_split_path_info ^(.+\.php)(/.+)$;
    fastcgi_pass unix:/run/php-fpm.sock;
    fastcgi_buffers 16 16k;
    fastcgi_buffer_size 32k;
    fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
    fastcgi_param SCRIPT_NAME $fastcgi_script_name;
    fastcgi_index index.php;
    include fastcgi_params;
    fastcgi_intercept_errors off;
    # fastcgi_read_timeout should match max_execution_time in php.ini
    fastcgi_read_timeout 10m;
    fastcgi_param SERVER_NAME $host;
    fastcgi_param HTTPS $fcgi_https;
}

# Expire rules for static content
# Media: images, icons, video, audio, HTC
location ~* \.(?:jpg|jpeg|gif|png|ico|cur|gz|svg|svgz|mp4|ogg|ogv|webm|htc)$ {
    expires 1M;
    access_log off;
    add_header Cache-Control "public";
}

# Prevent clients from accessing hidden files (starting with a dot)
# This is particularly important if you store .htpasswd files in the site hierarchy
# Access to `/.well-known/` is allowed.
# https://www.mnot.net/blog/2010/04/07/well-known
# https://tools.ietf.org/html/rfc5785
location ~* /\.(?!well-known\/) {
    deny all;
}

# Prevent clients from accessing to backup/config/source files
location ~* (?:\.(?:bak|conf|dist|fla|in[ci]|log|psd|sh|sql|sw[op])|~)$ {
    deny all;
}
```

to:
```
location /index.php {
    try_files /not_exists @swoole;
}

location / {
    try_files $uri $uri/ @swoole;
}

location @swoole {
    set $suffix "";

    if ($uri = /index.php) {
        set $suffix ?$query_string;
    }

    proxy_http_version 1.1;
    proxy_set_header Host $http_host;
    proxy_set_header Scheme $scheme;
    proxy_set_header SERVER_PORT $server_port;
    proxy_set_header REMOTE_ADDR $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;

    proxy_pass http://127.0.0.1:8000$suffix;
}
```

Where the most important line is: `proxy_pass http://127.0.0.1:8000$suffix;` The used port number is `8000`. Make sure that the proxy_pass port is the same as the one set in the Swoole module port configuration!
