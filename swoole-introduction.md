---
layout: page
title: Swoole Introduction
permalink: /swoole/introduction/
---
## Introduction

The Swoole module for Drupal supercharges your website's performance by serving it via the [Swoole](https://github.com/swoole/swoole-src) or the [OpenSwoole](https://github.com/openswoole/swoole-src) PHP server. Both are PHP extensions. Documentation for both extensions can be found [here](https://openswoole.com/). The (Open)Swoole PHP server boots Drupal once, keeps it in memory and then feeds it requests at supersonic speeds.

It is great for running your website in production, only it is not so great when you are developing your Drupal website. As the Swoole server is caching a lot of stuff, it needs to be rebooted every time a file changes or a new one is added, a menu item is added or changed or some configuration is added or changed. Rebooting the (Open)Swoole server every time will get old very quick.

My suggestion is when your website is using the (Open)Swoole server to add a second tradition webserver for a domain like "https://admin.YOUR_DOMAIN.com" to do administration activities. When your are done doing that, then restart the (Open)Swoole server.

## From [OpenSwoole.com](https://openswoole.com/)

Compared with other async programming frameworks or software such as Nginx, Tornado, Node.js, Open Swoole is a complete async solution that has built-in support for async programming via fibers/coroutines, a range of multi-threaded I/O modules (HTTP Server, WebSockets, TaskWorkers, Process Pools) and support for popular PHP clients like PDO for MySQL, Redis and CURL.

You can use sync or async, coroutine, fiber API to write the applications or create thousands of light weight fibers within one Linux process.

Swoole enhances the efficiency of your PHP applications and brings you out of the traditional stateless model, enabling you to focus on the development of innovative products at high scale, brining event loops and asynchronous programming to the PHP language.

 - Event-driven, PHP Coroutine, PHP Fiber, Asynchronous API
 - Async TCP / UDP / HTTP / Websocket / HTTP2 Client / Server Side API
 - IPv4 / IPv6 / UnixSocket / TCP / UDP and SSL / TLS / DTLS
 - Native PHP Coroutine and PHP Fiber Support
 - High performance, scalable, support C1000K
 - Milliseconds task scheduler
 - Free and Open Source ([Apache 2 License](https://www.apache.org/licenses/LICENSE-2.0.html))
 - Multiprocessing and Daemonize
