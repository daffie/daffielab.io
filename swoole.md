---
layout: page
title: Swoole
permalink: /swoole/
---

The (Open)Swoole PHP server boots Drupal once, keeps it in memory and then feeds it requests at supersonic speeds.

1. [Introduction](/swoole/introduction/)

2. [Installation](/swoole/installation/)

3. [Drush](/swoole/drush/)
